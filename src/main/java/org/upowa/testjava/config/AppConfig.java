package org.upowa.testjava.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Copyright (c) 2021, UpOwa, All Right Reserved.
 * https://www.upowa.energy
 * <p>
 * When : 27/07/2021 -- 18:41
 * By : @author alexk
 * Project : uland-servey-back
 * Package : org.upowa.ulandsurvey.config
 */

@Configuration
public class AppConfig {

    @Bean
    public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption, @Value("${application-version}") String appVersion) {
        return new OpenAPI()
                .addServersItem(new Server().url("/"))
                .info(new Info()
                        .title("Test Java API")
                        .version(appVersion)
                        .description(appDesciption)
                        .termsOfService("https://swagger.io/terms/")
                        .license(new License().name("Apache 2.0").url("https://springdoc.org")))
                .externalDocs(new ExternalDocumentation()
                        .description("Source code").url("https://gitlab.com/alex.kouasseu/test-java.git"));
    }
}
